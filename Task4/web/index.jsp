<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Vehicle</title>
  </head>
  <body>
    <form action="index.jsp">
        <input type="button" name="accelerate" value="Accelerate">
        <input type="button" name="brake" value="Braking">
        <input type="button" name="turnLeft" value="Turn left">
        <input type="button" name="turnRight" value="Turn Right">
        <input type="button" name="lightOn" value="Light On">
        <input type="button" name="lightOff" value="Light Off">
    </form>
  </body>
</html>
