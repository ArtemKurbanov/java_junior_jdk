package com.task4.java;


public class Car implements Drive {

    private int wheel;
    private int engine;
    private double gasTank;

    public double getGasTank() {
        return gasTank;
    }

    public void drive(){
        drivingCar();
        speedCar();
        System.out.println("Drive");
    }

    @Override
    public void accelerate() {
        System.out.println("Accelerate");
    }

    @Override
    public void brake() {
        System.out.println("Braking");
    }

    @Override
    public void lightOn() {
        System.out.println("LightOn");
    }

    @Override
    public void lightOff() {
        System.out.println("LightOff");
    }

    @Override
    public void turnLeft() {
        System.out.println("Turning Left");
    }

    @Override
    public void turnRight() {
        System.out.println("Turning Right");
    }

    private void speedCar() {
    }

    private void drivingCar() {
    }

}
