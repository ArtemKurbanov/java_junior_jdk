package com.task4.java;

public interface Drive {

    public void accelerate();
    public void brake();
    public void turnLeft();
    public void turnRight();
    public void lightOn();
    public void lightOff();
}
